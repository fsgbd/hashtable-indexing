package fr.miage.fsgbd;

/**
 * @author Grégory Galli
 */
public class Main {

    // La taille max de l'index
    public static final Integer HASHTABLE_SIZE = 50;

    // Le délimiteur à utiliser pour séparer les champs
    private static final String DELIMITER = ";";

    // Le nom du fichier, doit se trouver dans la racine du projet
    private static final String DATA_FILE_NAME = "data.csv";

    // Personnaliser ici les noms des colonnes que vous souhaitez utiliser pour la base de l'index
    private static final String[] INDEX_COLUMN_LIST = {"civilite","nom","prenom"};

    public static void main(String args[]){
        // On instancie une nouvelle hashtable
        HashtableIndex hashtableIndex = new HashtableIndex(HASHTABLE_SIZE);

        // On l'initialise avec les paramètres définis plus haut
        hashtableIndex.init(DATA_FILE_NAME, DELIMITER, INDEX_COLUMN_LIST);

    }
}
