Créé par Grégory Galli, le 31/05/2021

Ce projet a été créé tout spécialement pour illustrer le concept des indexes basés sur les tables de hashage

Le concept est très basique mais permets assez facilement de créer un index paramétrable sur plusieurs points :

- La taille de l'index (default = 50)
- Le nom du fichier CSV à parser (default = data.csv)
- Le délimiteur à utiliser pour séparer les champs du CSV (default = ;)
- Les colonnes sur lesquelles ils faut se baser pour créer l'index (default = {"civilite","nom","prenom"})

Toutes ces valeurs sont paramétrables dans les variables statiques définies dans la classe "fr.miage.fsgbd.Main"

A l'exécution, le CSV sera analysé en fonction des paramètres fournis et indexé dans une Hashtable.

Cette implémentation gère les collisions avec l'approche du chaînage externe.

Pas de licence particulière, si vous souhaitez réutiliser ce code, faites vous plaisir !

Pour toutes questions : greg.galli@gmail.com
